package com.pedrohlc.cacic.coffe;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final String baseURL = "http://cap.dc.ufscar.br/~726578/cgi-bin/coffe_";
    private Toast invalidName;

    private RatingBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        invalidName = Toast.makeText(this, "Qual seu nome cara?", Toast.LENGTH_LONG);

        findViewById(R.id.buttonEmpty).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick = ((EditText) findViewById(R.id.yourName)).getText().toString();
                if(nick.length() < 3) {
                    semNome();
                    return;
                }
                simpleReq(baseURL+"empty.cgi?nick="+nick);
                loadHistory();
            }
        });

        findViewById(R.id.buttonSugarPlease).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick = ((EditText) findViewById(R.id.yourName)).getText().toString();
                if(nick.length() < 3) {
                    semNome();
                    return;
                }
                simpleReq(baseURL+"nosugar.cgi?nick="+nick);
                loadHistory();
            }
        });

        findViewById(R.id.buttonDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick = ((EditText) findViewById(R.id.yourName)).getText().toString();
                if(nick.length() < 3) {
                    semNome();
                    return;
                }
                simpleReq(baseURL+"done.cgi?nick="+nick);
                loadHistory();
            }
        });

        findViewById(R.id.buttonReload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadHistory();
            }
        });

        bar = findViewById(R.id.ratingBar);
        findViewById(R.id.buttonRate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick = ((EditText) findViewById(R.id.yourName)).getText().toString();
                if(nick.length() < 3) {
                    semNome();
                    return;
                }
                simpleReq(baseURL+"rate.cgi?rate="+Math.round(bar.getRating())+"&nick="+nick);
                loadHistory();
            }
        });

        ((EditText)findViewById(R.id.yourName)).setFilters(new InputFilter[] { new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }

        } });

        loadHistory();
    }

    private void semNome() {
        invalidName.show();
    }

    public void loadHistory() {
        LinearLayout history = findViewById(R.id.history);

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) new URL(baseURL+"history.cgi").openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            JSONObject res = new JSONObject(reader.readLine());
            JSONArray arr = res.getJSONArray("entries");

            history.removeAllViewsInLayout();
            for (int i=0; i < arr.length(); i++) {
                JSONObject entry = arr.getJSONObject(i);
                String time = entry.getString("time");
                time = time.replace("-", ":");
                String action = entry.getString("action");
                TextView entryView = new TextView(this);
                entryView.setTag(R.id.tag0, time);
                switch (action){
                    case "empty":
                        entryView.setText(time + " - " + entry.getString("nick") + " não achou café.");
                        break;
                    case "done":
                        entryView.setText(time + " - " + entry.getString("nick") + " fez café.");
                        break;
                    case "rate":
                        entryView.setText(time + " - " + entry.getString("nick") + " deu nota " + entry.getString("grade") + " para o café.");
                        break;
                    default:
                        entryView.setText("INVALID DATA");
                }
                int pos;
                for(pos=0; pos<i; pos++)
                    if(time.compareTo((String)history.getChildAt(pos).getTag(R.id.tag0)) > 0)
                        break;
                history.addView(entryView, pos);
            }

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
    }

    void simpleReq(String urlStr) {
        try {
            new URL(urlStr).openConnection().getInputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
